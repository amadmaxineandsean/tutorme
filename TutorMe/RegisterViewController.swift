//
//  RegisterViewController.swift
//  TutorMe
//
//  Created by Maxine Tan on 20/1/18.
//  Copyright © 2018 Sean Yeo. All rights reserved.
//

import UIKit
import Firebase

class RegisterViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func dismiss_onClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func signUpButton(_ sender: Any) {
        Auth.auth().createUser(withEmail: "maxtan98@gmail.com", password: "12345") { (user, error) in
            if error != nil{
                print(error?.localizedDescription)
                return
            }
            print(user)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
